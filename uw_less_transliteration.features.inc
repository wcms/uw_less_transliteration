<?php

/**
 * @file
 * uw_less_transliteration.features.inc
 */

/**
 * Implements hook_strongarm_alter().
 */
function uw_less_transliteration_strongarm_alter(&$data) {
  if (isset($data['pathauto_reduce_ascii'])) {
    $data['pathauto_reduce_ascii']->value = 0; /* WAS: 1 */
  }
  if (isset($data['pathauto_transliterate'])) {
    $data['pathauto_transliterate']->value = 0; /* WAS: 1 */
  }
}
